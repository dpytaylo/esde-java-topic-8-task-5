package src;

public class Main {
    public static void main(String[] args) {
        var time = new Time(10, 24, 53);
        time.add(new Time(2, 2, 30));

        System.out.printf("%02d:%02d:%02d%n", time.getHours(), time.getMinutes(), time.getSeconds());

        time = new Time(10, 24, 53);
        time.sub(new Time(0, 0, 60));

        System.out.printf("%02d:%02d:%02d%n", time.getHours(), time.getMinutes(), time.getSeconds());
    }
}
